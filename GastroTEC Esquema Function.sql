USE GastroDB;

DROP function IF EXISTS funGetIdHorario;
DELIMITER $$
CREATE FUNCTION funGetIdHorario (
	DiaInicio   VARCHAR(10),
	DiaFin      VARCHAR(10),
	HoraInicio  TIME,
	HoraFin     TIME
)
RETURNS INTEGER
BEGIN
	DECLARE IdHorario INT DEFAULT 0;
	SELECT H.IdHorario INTO IdHorario
	FROM Horario H
    WHERE H.DiaInicio = DiaInicio AND H.DiaFin = DiaFin AND H.HoraInicio = HoraInicio AND H.HoraFIN = HoraFin;
RETURN IdHorario;
END$$
DELIMITER ;


DROP function IF EXISTS funGetIdRestauranteByNombre;
DELIMITER $$
CREATE FUNCTION funGetIdRestauranteByNombre (
Nombre VARCHAR(54))
RETURNS INTEGER
BEGIN
	DECLARE IdRestaurante INT DEFAULT 0;
    SELECT R.IdRestaurante INTO IdRestaurante
    FROM Restaurante R 
    WHERE R.Nombre=Nombre;
RETURN IdRestaurante;
END$$
DELIMITER ;


DROP function IF EXISTS funGetIdPlatilloByNombre;
DELIMITER $$
CREATE FUNCTION funGetIdPlatilloByNombre (
Nombre VARCHAR(54))
RETURNS INTEGER
BEGIN
	DECLARE IdPlatillo INT DEFAULT 0;
    SELECT P.IdPlatillo INTO IdPlatillo
    FROM Platillo P 
    WHERE P.Nombre=Nombre;
RETURN IdPlatillo;
END$$
DELIMITER ;


DROP function IF EXISTS funGetIdCarreraByNombre;
DELIMITER $$
CREATE FUNCTION funGetIdCarreraByNombre (
Nombre VARCHAR(54))
RETURNS INTEGER
BEGIN
	DECLARE IdCarrera INT DEFAULT 0;
    SELECT C.IdCarrera INTO IdCarrera
    FROM Carrera C 
    WHERE C.NombreinsertarCarrera=Nombre;
RETURN IdCarrera;
END$$
DELIMITER ;


DROP function IF EXISTS funGetIdUsuarioByCarnet;
DELIMITER $$
CREATE FUNCTION funGetIdUsuarioByCarnet (
Carnet INT)
RETURNS INTEGER
BEGIN
	DECLARE IdUsuario INT DEFAULT 0;
    SELECT U.IdUsuario INTO IdUsuario
    FROM Usuario U  
    WHERE U.Carnet=Carnet;
RETURN IdUsuario;
END$$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetUsuarioParticipativo;
DELIMITER $$
CREATE FUNCTION funGetUsuarioParticipativo()
RETURNS VARCHAR (45) 
DETERMINISTIC
BEGIN
	DECLARE nombremasparticipativo VARCHAR(45);
	DECLARE mayorparticipacion INT DEFAULT 0;
	SET mayorparticipacion = (SELECT MAX(Participacion) FROM Usuario);
	SELECT Nombre INTO nombremasparticipativo FROM Usuario WHERE Participacion=mayorparticipacion;
	RETURN nombremasparticipativo;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMasGustadoMes;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMasGustadoMes(
	mes INT,
    anno INT
)
RETURNS VARCHAR (50) 
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
    
	SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos 
    INTO idTemp, countTemp
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=1 AND MONTH(VP.Fecha)=mes AND YEAR(VP.Fecha)=anno
    GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMenosGustadoMes;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMenosGustadoMes(
	mes INT,
    anno INT
)
RETURNS VARCHAR (50) 
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
    
	SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos 
    INTO idTemp, countTemp
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=0 AND MONTH(VP.Fecha)=mes AND YEAR(VP.Fecha)=anno
    GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMasGustadoDia;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMasGustadoDia(
	dia INT,
    mes INT,
	anno INT
)
RETURNS VARCHAR (50) 
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
    
	SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos 
    INTO idTemp, countTemp
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=1 AND MONTH(VP.Fecha)=mes AND DAY(VP.Fecha)=dia AND YEAR(VP.Fecha)=anno
    GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMenosGustadoDia;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMenosGustadoDia(
	dia INT,
    mes INT,
	anno INT
)
RETURNS VARCHAR (50) 
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
    
	SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos 
    INTO idTemp, countTemp
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=0 AND MONTH(VP.Fecha)=mes AND DAY(VP.Fecha)=dia AND YEAR(VP.Fecha)=anno
    GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMenosGustado;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMenosGustado()
RETURNS VARCHAR (50)
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
    
	SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos 
    INTO idTemp, countTemp
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=0
    GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMasGustado;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMasGustado()
RETURNS VARCHAR (50)
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
        
    SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos
    INTO idTemp, countTemp
	FROM PlatilloRestaurante PR 
		INNER JOIN VotoPlatillo VP ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
	WHERE VP.Voto=1
	GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMasGustadoRestaurante;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMasGustadoRestaurante(
	idRestauranteTemp INT
)
RETURNS VARCHAR (50)
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
    
	SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos 
    INTO idTemp, countTemp
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=1 AND PR.IdRestaurante=idRestauranteTemp
    GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetPlatilloMenosGustadoRestaurante;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMenosGustadoRestaurante(
	idRestauranteTemp INT
)
RETURNS VARCHAR (50)
DETERMINISTIC
BEGIN
	DECLARE idTemp INT;
    DECLARE countTemp INT;
    DECLARE temp VARCHAR(50);
    
	SELECT PR.IdPlatillo, COUNT(VP.Voto) as votos 
    INTO idTemp, countTemp
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=0 AND PR.IdRestaurante=idRestauranteTemp
    GROUP BY PR.IdPlatillo ORDER BY votos DESC LIMIT 1;
    
    SELECT Nombre into temp FROM Platillo WHERE IdPlatillo=idTemp;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetHorarioFrecuente;
DELIMITER $$
CREATE FUNCTION funGetHorarioFrecuente()
RETURNS TIME
DETERMINISTIC
BEGIN
	DECLARE temp TIME;
    DECLARE cuentaTemp INT;
	SELECT COUNT(Hora) as cuenta, Hora
    INTO cuentaTemp,temp
    FROM Visita
    GROUP BY Hora ORDER by cuenta DESC LIMIT 1;
    return temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetDiaFrecuente;
DELIMITER $$
CREATE FUNCTION funGetDiaFrecuente()
RETURNS VARCHAR(20)
DETERMINISTIC
BEGIN
	DECLARE temp VARCHAR(20);
    DECLARE cuentaTemp INT;
    
	SELECT COUNT(Fecha) as cuenta, DAYNAME(Fecha)
    INTO cuentaTemp,temp
    FROM Visita
    GROUP BY Fecha ORDER by cuenta DESC LIMIT 1;
    
    return temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funGetCarreraFrecuente;
DELIMITER $$
CREATE FUNCTION funGetCarreraFrecuente(
	idRestaurante INT
)
RETURNS VARCHAR(20)
DETERMINISTIC
BEGIN
	DECLARE temp VARCHAR(20);
    DECLARE cuentaTemp INT;
	SELECT C.Nombre, COUNT(V.IdUsuario) as cuenta
    INTO temp,cuentaTemp
    FROM Visita V
		INNER JOIN Usuario U ON V.IdUsuario=U.IdUsuario
		INNER JOIN Carrera C ON C.IdCarrera = U.IdCarrera
	WHERE V.IdRestaurante=idRestaurante
	GROUP BY C.Nombre ORDER BY cuenta DESC LIMIT 1;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funExistVotoRestaurante;
DELIMITER $$
CREATE FUNCTION funExistVotoRestaurante(
	idUsuario INT,
    idRestaurante INT
)
RETURNS BOOL 
DETERMINISTIC
BEGIN
	DECLARE temp BOOL;
    DECLARE tempDate DATE;
    SELECT VR.Fecha INTO tempDate FROM VotoRestaurante VR WHERE VR.IdUsuario=idUsuario AND VR.IdRestaurante=idRestaurante ORDER BY VR.Fecha DESC LIMIT 1;
	IF YEAR(CURRENT_DATE())=YEAR(tempDate) THEN
		IF MONTH(CURRENT_DATE())<7 AND MONTH(tempDate)<7 
			THEN SET temp=0;
        ELSEIF MONTH(CURRENT_DATE()) >= 7 AND MONTH(tempDate) >= 7 
			THEN SET temp=0;
		ELSEIF MONTH(CURRENT_DATE()) = MONTH(tempDate) >= 7
			THEN SET temp=0;
        ELSE 
			SET temp=1;
        END IF;
    ELSE SET temp=1;
    END IF;
    RETURN temp;
END $$
DELIMITER ;


DROP FUNCTION IF EXISTS funExistHorario;
DELIMITER $$
CREATE FUNCTION funExistHorario(
	horaIncioTemp TIME,
	horaFinTemp TIME,
	diaInicioTemp VARCHAR(20),
	diaFinTemp VARCHAR(20)
)
RETURNS INT 
DETERMINISTIC
BEGIN
    DECLARE idTemp INT;
    SET idTemp=0;
    SELECT IdHorario INTO idTemp 
		FROM Horario 
		WHERE HoraInicio=horaIncioTemp AND HoraFin=horaFinTemp AND DiaInicio=diaInicioTemp AND DiaFin=diaFinTemp 
        LIMIT 1;
    RETURN idTemp;
END $$
DELIMITER ;


DROP function IF EXISTS funGetPlatilloMasVotadoSemana;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMasVotadoSemana (
)
RETURNS varchar (50)
BEGIN
	DECLARE tempId INT;
    DECLARE tempCount INT; 
    DECLARE tempNombre varchar (50);
    
	SELECT VP.IdPlatilloRestaurante, COUNT(VP.Voto) AS Votos
    INTO tempId, tempCount
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=1 AND VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante AND VP.Fecha>NOW()-INTERVAL 1 WEEK
    GROUP BY VP.IdPlatilloRestaurante ORDER BY votos DESC LIMIT 1;
    
    SELECT P.Nombre INTO tempNombre FROM Platillo P, PlatilloRestaurante PR WHERE PR.IdPlatilloRestaurante=tempId AND P.IdPlatillo=PR.IdPlatillo; 
	
RETURN tempNombre;
END$$
DELIMITER ;


DROP function IF EXISTS funGetPlatilloMenosVotadoSemana;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMenosVotadoSemana (
)
RETURNS varchar (50)
BEGIN
	DECLARE tempId INT;
    DECLARE tempCount INT; 
    DECLARE tempNombre varchar (50);
    
	SELECT VP.IdPlatilloRestaurante, COUNT(VP.Voto) AS Votos
    INTO tempId, tempCount
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=0 AND VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante AND VP.Fecha>NOW()-INTERVAL 1 WEEK
    GROUP BY VP.IdPlatilloRestaurante ORDER BY votos DESC LIMIT 1;
    
    SELECT P.Nombre INTO tempNombre FROM Platillo P, PlatilloRestaurante PR WHERE PR.IdPlatilloRestaurante=tempId AND P.IdPlatillo=PR.IdPlatillo; 
	
RETURN tempNombre;
END$$
DELIMITER ;


DROP function IF EXISTS funGetPlatilloMasVotadoSemestre;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMasVotadoSemestre (
)
RETURNS varchar (50)
BEGIN
	DECLARE tempId INT;
    DECLARE tempCount INT; 
    DECLARE tempNombre varchar (50);
    
	SELECT VP.IdPlatilloRestaurante, COUNT(VP.Voto) AS Votos
    INTO tempId, tempCount
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=1 AND VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante AND VP.Fecha>NOW()-INTERVAL 6 month
    GROUP BY VP.IdPlatilloRestaurante ORDER BY votos DESC LIMIT 1;
    
    SELECT P.Nombre INTO tempNombre FROM Platillo P, PlatilloRestaurante PR WHERE PR.IdPlatilloRestaurante=tempId AND P.IdPlatillo=PR.IdPlatillo; 
	
RETURN tempNombre;
END$$
DELIMITER ;

DROP function IF EXISTS funGetPlatilloMenosVotadoSemestre;
DELIMITER $$
CREATE FUNCTION funGetPlatilloMenosVotadoSemestre (
)
RETURNS varchar (50)
BEGIN
	DECLARE tempId INT;
    DECLARE tempCount INT; 
    DECLARE tempNombre varchar (50);
    
	SELECT VP.IdPlatilloRestaurante, COUNT(VP.Voto) AS Votos
    INTO tempId, tempCount
    FROM VotoPlatillo VP INNER JOIN PlatilloRestaurante PR ON VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante
    WHERE VP.Voto=0 AND VP.IdPlatilloRestaurante=PR.IdPlatilloRestaurante AND VP.Fecha>NOW()-INTERVAL 6 month
    GROUP BY VP.IdPlatilloRestaurante ORDER BY votos DESC LIMIT 1;
    
    SELECT P.Nombre INTO tempNombre FROM Platillo P, PlatilloRestaurante PR WHERE PR.IdPlatilloRestaurante=tempId AND P.IdPlatillo=PR.IdPlatillo; 
	
RETURN tempNombre;
END$$
DELIMITER ;
