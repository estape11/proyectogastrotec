CREATE DATABASE IF NOT EXISTS GastroDB;
USE GastroDB;

-- DROP DATABASE GastroDB;

CREATE TABLE IF NOT EXISTS Horario (
            IdHorario INT PRIMARY KEY AUTO_INCREMENT,
            HoraInicio TIME NOT NULL,
            HoraFin TIME NOT NULL,
            DiaInicio VARCHAR(20) NOT NULL,
            DiaFin VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS Restaurante (
            IdRestaurante INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL UNIQUE,
            Descripcion VARCHAR(250) NOT NULL,
            Ubicacion VARCHAR(30) NOT NULL,
            IdHorario INT NOT NULL,
			FOREIGN KEY (IdHorario) REFERENCES Horario(IdHorario)
);

CREATE TABLE IF NOT EXISTS Administrador (
            IdAministrador INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL,
            Apellido VARCHAR(50) NOT NULL,
            Email VARCHAR(50) NOT NULL UNIQUE,
            Pass VARCHAR(10) NOT NULL,
            IdRestaurante INT NOT NULL,
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante(IdRestaurante)
);

CREATE TABLE IF NOT EXISTS Carrera (
            IdCarrera INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS Usuario (
            IdUsuario INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL,
            Apellido VARCHAR(50) NOT NULL,
            Email VARCHAR(50) UNIQUE NOT NULL,
            Pass VARCHAR(50) NOT NULL,
            Carnet INT UNIQUE NOT NULL,
            Participacion INT NOT NULL,
            IdCarrera INT NOT NULL,
            FOREIGN KEY (IdCarrera) REFERENCES Carrera(IdCarrera)
);

CREATE TABLE IF NOT EXISTS Platillo (
            IdPlatillo INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL UNIQUE,
            Descripcion VARCHAR(250) NOT NULL
);

CREATE TABLE IF NOT EXISTS PlatilloRestaurante(
            IdPlatilloRestaurante INT PRIMARY KEY AUTO_INCREMENT,
            IdPlatillo INT NOT NULL,
            IdRestaurante INT NOT NULL,
            Precio INT NOT NULL,
            Fecha DATE NOT NULL,
            Hora TIME NOT NULL,
            FOREIGN KEY (IdPlatillo) REFERENCES Platillo(IdPlatillo),
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante(IdRestaurante) 
);

CREATE TABLE IF NOT EXISTS VotoPlatillo (
            IdPlatilloRestaurante INT NOT NULL,
            IdUsuario INT NOT NULL,
            Fecha DATE NOT NULL,
            Voto TINYINT NOT NULL,
            FOREIGN KEY (IdPlatilloRestaurante) REFERENCES PlatilloRestaurante(IdPlatilloRestaurante),
            FOREIGN KEY (IdUsuario) REFERENCES Usuario (IdUsuario)
);

CREATE TABLE IF NOT EXISTS VotoRestaurante (
            IdRestaurante INT NOT NULL,
            IdUsuario INT NOT NULL,
            Fecha DATE NOT NULL,
            Voto TINYINT NOT NULL,
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante (IdRestaurante),
            FOREIGN KEY (IdUsuario) REFERENCES Usuario (IdUsuario)
);

CREATE TABLE IF NOT EXISTS Visita (
            IdRestaurante INT NOT NULL,
            Fecha DATE NOT NULL,
            Hora TIME NOT NULL,
            IdUsuario INT NOT NULL,
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante(IdRestaurante),
            FOREIGN KEY (IdUsuario) REFERENCES Usuario(IdUsuario)
);