CREATE DATABASE IF NOT EXISTS GastroDB;
USE GastroDB;

-- DROP DATABASE GastroDB;

CREATE TABLE IF NOT EXISTS Horario (
            IdHorario INT PRIMARY KEY AUTO_INCREMENT,
            HoraInicio TIME NOT NULL,
            HoraFin TIME NOT NULL,
            DiaInicio VARCHAR(20) NOT NULL,
            DiaFin VARCHAR(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS Restaurante (
            IdRestaurante INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL UNIQUE,
            Descripcion VARCHAR(250) NOT NULL,
            Ubicacion VARCHAR(30) NOT NULL,
            IdHorario INT NOT NULL,
			FOREIGN KEY (IdHorario) REFERENCES Horario(IdHorario)
);

CREATE TABLE IF NOT EXISTS Administrador (
            IdAministrador INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL,
            Apellido VARCHAR(50) NOT NULL,
            Email VARCHAR(50) NOT NULL UNIQUE,
            Pass VARCHAR(10) NOT NULL,
            IdRestaurante INT NOT NULL,
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante(IdRestaurante)
);

CREATE TABLE IF NOT EXISTS Carrera (
            IdCarrera INT PRIMARY KEY AUTO_INCREMENT,
            NombreinsertarCarrera VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS Usuario (
            IdUsuario INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL,
            Apellido VARCHAR(50) NOT NULL,
            Email VARCHAR(50) UNIQUE NOT NULL,
            Pass VARCHAR(50) NOT NULL,
            Carnet INT UNIQUE NOT NULL,
            Participacion INT NOT NULL,
            IdCarrera INT NOT NULL,
            FOREIGN KEY (IdCarrera) REFERENCES Carrera(IdCarrera)
);

CREATE TABLE IF NOT EXISTS Platillo (
            IdPlatillo INT PRIMARY KEY AUTO_INCREMENT,
            Nombre VARCHAR(50) NOT NULL UNIQUE,
            Descripcion VARCHAR(250) NOT NULL
);

CREATE TABLE IF NOT EXISTS PlatilloRestaurante(
            IdPlatilloRestaurante INT PRIMARY KEY AUTO_INCREMENT,
            IdPlatillo INT NOT NULL,
            IdRestaurante INT NOT NULL,
            Precio INT NOT NULL,
            Fecha DATE NOT NULL,
            Hora TIME NOT NULL,
            FOREIGN KEY (IdPlatillo) REFERENCES Platillo(IdPlatillo),
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante(IdRestaurante) 
);

CREATE TABLE IF NOT EXISTS VotoPlatillo (
            IdPlatillo INT NOT NULL,
            IdUsuario INT NOT NULL,
            Fecha DATE NOT NULL,
            Voto TINYINT NOT NULL,
            FOREIGN KEY (IdPlatillo) REFERENCES PlatilloRestaurante(IdPlatilloRestaurante),
            FOREIGN KEY (IdUsuario) REFERENCES Usuario (IdUsuario)
);

CREATE TABLE IF NOT EXISTS VotoRestaurante (
            IdRestaurante INT NOT NULL,
            IdUsuario INT NOT NULL,
            Fecha DATE NOT NULL,
            VOTO TINYINT NOT NULL,
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante (IdRestaurante),
            FOREIGN KEY (IdUsuario) REFERENCES Usuario (IdUsuario)
);

CREATE TABLE IF NOT EXISTS Visita (
            IdRestaurante INT NOT NULL,
            Fecha DATE NOT NULL,
            Hora TIME NOT NULL,
            IdUsuario INT NOT NULL,
            FOREIGN KEY (IdRestaurante) REFERENCES Restaurante(IdRestaurante),
            FOREIGN KEY (IdUsuario) REFERENCES Usuario(IdUsuario)
);


-- PROCEDURES --
DROP PROCEDURE IF EXISTS insertarCarrera;
DELIMITER $$
CREATE PROCEDURE insertarCarrera(
	IN nombreTemp VARCHAR(50)
)
BEGIN
	INSERT INTO Carrera(NombreinsertarCarrera)
    VALUES(nombreTemp);
END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS insertarHorario;
DELIMITER $$
CREATE PROCEDURE insertarHorario(
	IN horaIncioTemp TIME,
	IN horaFinTemp TIME,
	IN diaInicioTemp VARCHAR(20),
	IN diaFinTemp VARCHAR(20)
)
BEGIN
	INSERT INTO Horario(HoraInicio,HoraFin,DiaInicio,DiaFin)
    VALUES(horaIncioTemp,horaFinTemp,diaInicioTemp,diaFinTemp);
END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS insertarRestaurante;
DELIMITER $$
CREATE PROCEDURE insertarRestaurante(
	IN nombreTemp VARCHAR(50),
	IN descripcionTemp VARCHAR(250),
	IN ubicacionTemp VARCHAR(30),
	IN idHorarioTemp INT
)
BEGIN
	INSERT INTO Restaurante(Nombre,Descripcion,Ubicacion,IdHorario)
    VALUES(nombreTemp,descripcionTemp,ubicacionTemp,idHorarioTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS insertarAdministrador;
DELIMITER $$
CREATE PROCEDURE insertarAdministrador(
	IN nombreTemp VARCHAR(50),
	IN apellidoTemp VARCHAR(50),
	IN emailTemp VARCHAR(50),
	IN passTemp VARCHAR(10),
	In idRestauranteTemp INT
)
BEGIN
	INSERT INTO Administrador(Nombre,Apellido,Email,Pass,IdRestaurante)
    VALUES(nombreTemp,apellidoTemp,emailTemp,passTemp,idRestauranteTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS insertarPlatillo;
DELIMITER $$
CREATE PROCEDURE insertarPlatillo(
	IN nombreTemp VARCHAR(50),
    IN descripcionTemp VARCHAR(250)
)
BEGIN
	INSERT INTO Platillo(Nombre,Descripcion)
    VALUES(nombreTemp,descripcionTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS insertarUsuario;
DELIMITER $$
CREATE PROCEDURE insertarUsuario(
	IN nombreTemp VARCHAR(50),
	IN apellidoTemp VARCHAR(50),
	IN emailTemp VARCHAR(50),
	IN passTemp VARCHAR(50),
	IN carnetTemp INT,
	IN participacionTemp INT,
	IN idCarreraTemp INT
)
BEGIN
	INSERT INTO Usuario(Nombre,Apellido,Email,Pass,Carnet,Participacion,IdCarrera)
    VALUES(nombreTemp,apellidoTemp,emailTemp,passTemp,carnetTemp,participacionTemp,idCarreraTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS insertarPlatilloRestaurante;
DELIMITER $$
CREATE PROCEDURE insertarPlatilloRestaurante(
	IN idPlatilloTemp INT,
	IN idRestauranteTemp INT,
	IN precioTemp INT,
	IN fechaTemp DATE,
	IN horaTemp TIME
)
BEGIN
	INSERT INTO PlatilloRestaurante(IdPlatillo,IdRestaurante,Precio,Fecha,Hora)
    VALUES(idPlatilloTemp,idRestauranteTemp,precioTemp,fechaTemp,horaTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS insertarVotoPlatillo;
DELIMITER $$
CREATE PROCEDURE insertarVotoPlatillo(
	IN idPlatilloTemp INT,
	IN idUsuarioTemp INT,
	IN fechaTemp DATE,
	IN votoTemp TINYINT
)
BEGIN
	INSERT INTO VotoPlatillo(IdPlatillo,IdUsuario,Fecha,Voto)
    VALUES(idPlatilloTemp,idUsuarioTemp,fechaTemp,votoTemp);
	UPDATE Usuario
	SET Participacion=Participacion+1
	WHERE Usuario.IdUsuario = idUsuarioTemp;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS insertarVotoRestaurante;
DELIMITER $$
CREATE PROCEDURE insertarVotoRestaurante(
	IN idRestauranteTemp INT,
	IN idUsuarioTemp INT,
	IN fechaTemp DATE,
	IN votoTemp TINYINT
)
BEGIN
	INSERT INTO VotoRestaurante(IdRestaurante,IdUsuario,Fecha,Voto)
    VALUES(idRestauranteTemp,idUsuarioTemp,fechaTemp,votoTemp);
	UPDATE Usuario
	SET Participacion=Participacion+1
	WHERE Usuario.IdUsuario = idUsuarioTemp;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS insertarVisita;
DELIMITER $$
CREATE PROCEDURE insertarVisita(
	IN idRestauranteTemp INT,
	IN fechaTemp DATE,
	IN horaTemp TIME,
	IN idUsuarioTemp INT
)
BEGIN
	INSERT INTO Visita(IdRestaurante,Fecha,Hora,IdUsuario)
    VALUES(idRestauranteTemp,fechaTemp,horaTemp,idUsuarioTemp);
    UPDATE Usuario
	SET Participacion=Participacion+1
	WHERE Usuario.IdUsuario = idUsuarioTemp;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarHorario;
DELIMITER $$
CREATE PROCEDURE generarHorario()
BEGIN
	CALL insertarHorario("08:00:00","20:00:00","Lunes","Viernes");
    CALL insertarHorario("07:30:00","19:00:00","Lunes","Viernes");
    CALL insertarHorario("08:00:00","19:00:00","Lunes","Viernes");
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarRestaurante;
DELIMITER $$
CREATE PROCEDURE generarRestaurante()
BEGIN
	CALL insertarRestaurante("Casa Luna","Comida para todo momento","Edificio K8",1);
    CALL insertarRestaurante("Soda El Lago","Cafe, batidos y mas","Edificio J1",3);
    CALL insertarRestaurante("Comedor Institucional","Comida accesible para todos","Edificio C1",2);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarAdministrador;
DELIMITER $$
CREATE PROCEDURE generarAdministrador()
BEGIN
	CALL isertarAdministrador("Cesar","Barrantes","lago@gmail.com","lago123",2);
    CALL isertarAdministrador("Patricia","Perez","comedor@gmail.com","come123",3);
    CALL isertarAdministrador("Vera","Mora","luna@gmail.com","luna123",1);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarCarrera;
DELIMITER $$
CREATE PROCEDURE generarCarrera()
BEGIN
CALL insertarCarrera("Ing.Computadores");
CALL insertarCarrera("Ing.Electronica");
CALL insertarCarrera("Ing.Computacion");
CALL insertarCarrera("Ing.Mecatronica");
CALL insertarCarrera("Ing.Mantenimiento");
CALL insertarCarrera("Ing.Materiales");
CALL insertarCarrera("Ing.Produccion");
CALL insertarCarrera("Ing.Diseño");
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarUsuario;
DELIMITER $$
CREATE PROCEDURE generarUsuario()
BEGIN
CALL insertarUsuario("Daniela","Hernandez","dani@gmail.com","abc",2015029225,0,1);
CALL insertarUsuario("Alejandra","Castrillo","ale@gmail.com","abc",2015155759,0,1);
CALL insertarUsuario("Esteban","Aguero","estape11@gmail.com","abc",2015097708,0,1);
CALL insertarUsuario("Fiorella","Ulloa","fio@gmail.com","abc",2015023226,0,6);
CALL insertarUsuario("Natalia","Morera","nati@gmail.com","abc",2015056431,0,8);
CALL insertarUsuario("Miguel","Jimenez","migue@gmail.com","abc",2016102654,0,1);
CALL insertarUsuario("Bernal","Villegas","berny@gmail.com","abc",2015014369,0,5);
CALL insertarUsuario("Juan","Solano","js@gmail.com","abc",2015123456,0,4);
END $$ 
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarPlatillo;
DELIMITER $$
CREATE PROCEDURE generarPlatillo()
BEGIN
CALL insertarPlatillo("Pinto","Pinto con huevo y platano maduro");
CALL insertarPlatillo("Emparedado","Emparedado de pollo");
CALL insertarPlatillo("Frutas","Taza con frutas mixtas de la temporada");
CALL insertarPlatillo("Pasta","Spaghetti con salsa de tomate y pollo");
CALL insertarPlatillo("Sopa Azteca","Sopa de tomate con tortillas");
CALL insertarPlatillo("Arepas","Arepas con miel");
CALL insertarPlatillo("Mini pizzas","Pizzas pequenas con salsa de tomate, peperoni y queso");
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarPlatilloRestaurante;
DELIMITER $$
CREATE PROCEDURE generarPlatilloRestaurante()
BEGIN
CALL insertarPlatilloRestaurante(1,2,1500,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"09:00:00"); 
CALL insertarPlatilloRestaurante(1,3,2000,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"09:00:00"); 
CALL insertarPlatilloRestaurante(1,4,500,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"09:00:00"); 
CALL insertarPlatilloRestaurante(2,2,2000,STR_TO_DATE("20-06-2017","%d-%m-%Y"),"04:00:00"); 
CALL insertarPlatilloRestaurante(2,4,500,STR_TO_DATE("20-06-2017","%d-%m-%Y"),"09:00:00"); 
CALL insertarPlatilloRestaurante(3,3,1000,STR_TO_DATE("22-08-2017","%d-%m-%Y"),"07:30:00"); 
CALL insertarPlatilloRestaurante(3,4,600,STR_TO_DATE("23-09-2017","%d-%m-%Y"),"09:00:00"); 
CALL insertarPlatilloRestaurante(4,2,2000,STR_TO_DATE("15-05-2017","%d-%m-%Y"),"11:20:00"); 
CALL insertarPlatilloRestaurante(4,3,2300,STR_TO_DATE("19-03-2017","%d-%m-%Y"),"11:20:00"); 
CALL insertarPlatilloRestaurante(5,4,700,STR_TO_DATE("05-03-2017","%d-%m-%Y"),"11:20:00"); 
CALL insertarPlatilloRestaurante(6,4,300,STR_TO_DATE("21-04-2017","%d-%m-%Y"),"9:00:00"); 
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS generarVisita;
DELIMITER $$
CREATE PROCEDURE generarVisita()
BEGIN
CALL insertarVisita(2,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"11:30:00",1);
CALL insertarVisita(3,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"3:00:00",2);
CALL insertarVisita(4,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"08:00:00",3);
CALL insertarVisita(4,STR_TO_DATE("29-09-2017","%d-%m-%Y"),"11:30:00",5);
CALL insertarVisita(4,STR_TO_DATE("29-05-2017","%d-%m-%Y"),"3:00:00",5);
CALL insertarVisita(3,STR_TO_DATE("29-09-2017","%d-%m-%Y"),"5:00:00",6);
CALL insertarVisita(2,STR_TO_DATE("26-05-2017","%d-%m-%Y"),"3:00:00",8);
CALL insertarVisita(2,STR_TO_DATE("29-05-2017","%d-%m-%Y"),"4:00:00",2);
CALL insertarVisita(3,STR_TO_DATE("29-09-2017","%d-%m-%Y"),"3:00:00",2);
SELECT * FROM Visita ORDER BY IdRestaurante ASC;
END $$
<<<<<<< HEAD
DELIMITER ; 
=======

DROP PROCEDURE IF EXISTS generarVotoPlatillo;
DELIMITER $$
CREATE PROCEDURE generarVotoPlatillo()
BEGIN
CALL insertarVotoPlatillo(3,1,STR_TO_DATE("23-09-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(4,2,STR_TO_DATE("15-05-2017","%d-%m-%Y"),0);
CALL insertarVotoPlatillo(4,3,STR_TO_DATE("15-05-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(4,4,STR_TO_DATE("19-03-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(3,5,STR_TO_DATE("22-09-2017","%d-%m-%Y"),0);
CALL insertarVotoPlatillo(2,6,STR_TO_DATE("20-06-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(2,7,STR_TO_DATE("20-06-2017","%d-%m-%Y"),0);
CALL insertarVotoPlatillo(3,8,STR_TO_DATE("23-09-2017","%d-%m-%Y"),0);
CALL insertarVotoPlatillo(1,3,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(5,4,STR_TO_DATE("05-03-2017","%d-%m-%Y"),0);
CALL insertarVotoPlatillo(6,5,STR_TO_DATE("21-04-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(2,6,STR_TO_DATE("20-06-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(1,1,STR_TO_DATE("23-06-2017","%d-%m-%Y"),0);
CALL insertarVotoPlatillo(1,8,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
CALL insertarVotoPlatillo(7,7,STR_TO_DATE("29-05-2017","%d-%m-%Y"),0);
CALL insertarVotoPlatillo(7,7,STR_TO_DATE("29-09-2017","%d-%m-%Y"),0);
SELECT * FROM VotoPlatillo ORDER BY IdPlatillo ASC;
END $$

DROP PROCEDURE IF EXISTS generarVotoRestaurante;
DELIMITER $$
CREATE PROCEDURE generarVotoRestaurante()
BEGIN
CALL insertarVotoRestaurante(2,1,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
CALL insertarVotoRestaurante(3,2,STR_TO_DATE("23-06-2017","%d-%m-%Y"),0);
CALL insertarVotoRestaurante(4,3,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
CALL insertarVotoRestaurante(4,5,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
CALL insertarVotoRestaurante(4,5,STR_TO_DATE("29-05-2017","%d-%m-%Y"),0);
CALL insertarVotoRestaurante(3,6,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
CALL insertarVotoRestaurante(2,8,STR_TO_DATE("26-05-2017","%d-%m-%Y"),0);
CALL insertarVotoRestaurante(2,2,STR_TO_DATE("29-05-2017","%d-%m-%Y"),1);
CALL insertarVotoRestaurante(3,2,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
SELECT * FROM VotoRestaurante ORDER BY IdRestaurante ASC;
END $$

>>>>>>> 9c5552f46c14be3e9a8a8dbebe0f15d165ff8fb1
-- SELECT * FROM Visita;
-- CALL insertarHorario("08:00:00","20:00:00","Lunes","Viernes");
-- SELECT * FROM Horario;


-- FUNCIONES --

USE `gastrodb`;
DROP function IF EXISTS `funGetIdHorario`;
DELIMITER $$
USE `gastrodb`$$
CREATE FUNCTION `funGetIdHorario` (
	DiaInicio   VARCHAR(10),
	DiaFin      VARCHAR(10),
	HoraInicio  TIME,
	HoraFin     TIME
)
RETURNS INTEGER
BEGIN
	DECLARE IdHorario INT DEFAULT 0;
	SELECT H.IdHorario INTO IdHorario
	FROM Horario H
    WHERE H.DiaInicio = DiaInicio AND H.DiaFin = DiaFin AND H.HoraInicio = HoraInicio AND H.HoraFIN = HoraFin;
RETURN IdHorario;
END$$
<<<<<<< HEAD
DELIMITER ;


USE `gastrodb`;
DROP function IF EXISTS `funGetIdRestauranteByNombre`;
DELIMITER $$
USE `gastrodb`$$
CREATE FUNCTION `funGetIdRestauranteByNombre` (
Nombre VARCHAR(54))
RETURNS INTEGER
BEGIN
	DECLARE IdRestaurante INT DEFAULT 0;
    SELECT R.IdRestaurante INTO IdRestaurante
    FROM Restaurante R 
    WHERE R.Nombre=Nombre;
RETURN IdRestaurante;
END$$
=======
CALL funGetIdHorario;
>>>>>>> 9c5552f46c14be3e9a8a8dbebe0f15d165ff8fb1
DELIMITER ;


USE `gastrodb`;
DROP function IF EXISTS `funGetIdPlatilloByNombre`;
DELIMITER $$
USE `gastrodb`$$
CREATE FUNCTION `funGetIdPlatilloByNombre` (
Nombre VARCHAR(54))
RETURNS INTEGER
BEGIN
	DECLARE IdPlatillo INT DEFAULT 0;
    SELECT P.IdPlatillo INTO IdPlatillo
    FROM Platillo P 
    WHERE P.Nombre=Nombre;
RETURN IdPlatillo;
END$$
DELIMITER ;


USE `gastrodb`;
DROP function IF EXISTS `funGetIdCarreraByNombre`;
DELIMITER $$
USE `gastrodb`$$
CREATE FUNCTION `funGetIdCarreraByNombre` (
Nombre VARCHAR(54))
RETURNS INTEGER
BEGIN
	DECLARE IdCarrera INT DEFAULT 0;
    SELECT C.IdCarrera INTO IdCarrera
    FROM Carrera C 
    WHERE C.NombreinsertarCarrera=Nombre;
RETURN IdCarrera;
END$$
DELIMITER ;


USE `gastrodb`;
DROP function IF EXISTS `funGetIdUsuarioByCarnet`;
DELIMITER $$
USE `gastrodb`$$
CREATE FUNCTION `funGetIdUsuarioByCarnet` (
Carnet INT)
RETURNS INTEGER
BEGIN
	DECLARE IdUsuario INT DEFAULT 0;
    SELECT U.IdUsuario INTO IdUsuario
    FROM Usuario U  
    WHERE U.Carnet=Carnet;
RETURN IdUsuario;
END$$
DELIMITER ;



CALL generarHorario();
SELECT funGetIdHorario("Lunes","Viernes","08:00:00","20:00:00");

<<<<<<< HEAD
CALL generarRestaurante();
SELECT funGetIdRestauranteByNombre("Casa Luna");

CALL generarPlatillo();
SELECT funGetIdPlatilloByNombre("Pasta");

CALL generarCarrera();
SELECT funGetIdCarreraByNombre("Ing.Mecatronica");

CALL generarUsuario();
SELECT funGetIdUsuarioByCarnet(2015155759);

=======
DROP FUNCTION IF EXISTS funGetUsuariParticipativo;
DELIMITER $$
CREATE FUNCTION funGetUsuariParticipativo()
RETURNS VARCHAR (45) 
DETERMINISTIC
BEGIN
DECLARE nombremasparticipativo VARCHAR(45);
DECLARE mayorparticipacion INT DEFAULT 0;
SET mayorparticipacion = (SELECT MAX(Participacion) FROM Usuario);
SELECT Nombre INTO nombremasparticipativo FROM Usuario WHERE Participacion=mayorparticipacion;
RETURN nombremasparticipativo;
END $$

SELECT funGetUsuariParticipativo();
>>>>>>> 9c5552f46c14be3e9a8a8dbebe0f15d165ff8fb1
