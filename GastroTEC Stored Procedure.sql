USE GastroDB;

DROP PROCEDURE IF EXISTS spAddCarrera;
DELIMITER $$
CREATE PROCEDURE spAddCarrera(
	IN nombreTemp VARCHAR(50)
)
BEGIN
	INSERT INTO Carrera(NombreinsertarCarrera)
    VALUES(nombreTemp);
END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS spAddHorario;
DELIMITER $$
CREATE PROCEDURE spAddHorario(
	IN horaIncioTemp TIME,
	IN horaFinTemp TIME,
	IN diaInicioTemp VARCHAR(20),
	IN diaFinTemp VARCHAR(20)
)
BEGIN
	INSERT INTO Horario(HoraInicio,HoraFin,DiaInicio,DiaFin)
    VALUES(horaIncioTemp,horaFinTemp,diaInicioTemp,diaFinTemp);
END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS spAddRestaurante;
DELIMITER $$
CREATE PROCEDURE spAddRestaurante(
	IN nombreTemp VARCHAR(50),
	IN descripcionTemp VARCHAR(250),
	IN ubicacionTemp VARCHAR(30),
	IN idHorarioTemp INT
)
BEGIN
	INSERT INTO Restaurante(Nombre,Descripcion,Ubicacion,IdHorario)
    VALUES(nombreTemp,descripcionTemp,ubicacionTemp,idHorarioTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddRestauranteHorario;
DELIMITER $$
CREATE PROCEDURE spAddRestauranteHorario(
	IN nombreTemp VARCHAR(50),
	IN descripcionTemp VARCHAR(250),
	IN ubicacionTemp VARCHAR(30),
	IN horaIncioTemp TIME,
	IN horaFinTemp TIME,
	IN diaInicioTemp VARCHAR(20),
	IN diaFinTemp VARCHAR(20)
)
BEGIN
	DECLARE idTemp INT;
    SELECT funExistHorario(horaIncioTemp,horaFinTemp,diaInicioTemp,diaFinTemp) INTO idTemp;
	IF idTemp!=0 THEN
		INSERT INTO Restaurante(Nombre,Descripcion,Ubicacion,IdHorario)
		VALUES(nombreTemp,descripcionTemp,ubicacionTemp,idTemp);
	ELSE 
		CALL spAddHorario(horaIncioTemp,horaFinTemp,diaInicioTemp,diaFinTemp);
        SET idTemp=LAST_INSERT_ID();
        INSERT INTO Restaurante(Nombre,Descripcion,Ubicacion,IdHorario)
		VALUES(nombreTemp,descripcionTemp,ubicacionTemp,idTemp);
	END IF;        
END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS spAddAdministrador;
DELIMITER $$
CREATE PROCEDURE spAddAdministrador(
	IN nombreTemp VARCHAR(50),
	IN apellidoTemp VARCHAR(50),
	IN emailTemp VARCHAR(50),
	IN passTemp VARCHAR(10),
	In idRestauranteTemp INT
)
BEGIN
	INSERT INTO Administrador(Nombre,Apellido,Email,Pass,IdRestaurante)
    VALUES(nombreTemp,apellidoTemp,emailTemp,passTemp,idRestauranteTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddPlatillo;
DELIMITER $$
CREATE PROCEDURE spAddPlatillo(
	IN nombreTemp VARCHAR(50),
    IN descripcionTemp VARCHAR(250)
)
BEGIN
	INSERT INTO Platillo(Nombre,Descripcion)
    VALUES(nombreTemp,descripcionTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddUsuario;
DELIMITER $$
CREATE PROCEDURE spAddUsuario(
	IN nombreTemp VARCHAR(50),
	IN apellidoTemp VARCHAR(50),
	IN emailTemp VARCHAR(50),
	IN passTemp VARCHAR(50),
	IN carnetTemp INT,
	IN participacionTemp INT,
	IN idCarreraTemp INT
)
BEGIN
	INSERT INTO Usuario(Nombre,Apellido,Email,Pass,Carnet,Participacion,IdCarrera)
    VALUES(nombreTemp,apellidoTemp,emailTemp,passTemp,carnetTemp,participacionTemp,idCarreraTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddPlatilloRestaurante;
DELIMITER $$
CREATE PROCEDURE spAddPlatilloRestaurante(
	IN idPlatilloTemp INT,
	IN idRestauranteTemp INT,
	IN precioTemp INT,
	IN fechaTemp DATE,
	IN horaTemp TIME
)
BEGIN
	INSERT INTO PlatilloRestaurante(IdPlatillo,IdRestaurante,Precio,Fecha,Hora)
    VALUES(idPlatilloTemp,idRestauranteTemp,precioTemp,fechaTemp,horaTemp);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddVotoPlatillo;
DELIMITER $$
CREATE PROCEDURE spAddVotoPlatillo(
	IN idPlatilloTemp INT,
	IN idUsuarioTemp INT,
	IN votoTemp TINYINT
)
BEGIN
	INSERT INTO VotoPlatillo(IdPlatilloRestaurante,IdUsuario,Fecha,Voto)
    VALUES(idPlatilloTemp,idUsuarioTemp,CURRENT_DATE(),votoTemp);
	UPDATE Usuario
	SET Participacion=Participacion+1
	WHERE Usuario.IdUsuario = idUsuarioTemp;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddVotoPlatilloFecha;
DELIMITER $$
CREATE PROCEDURE spAddVotoPlatilloFecha(
	IN idPlatilloTemp INT,
	IN idUsuarioTemp INT,
    IN fechaTemp DATE,
	IN votoTemp TINYINT
)
BEGIN
	INSERT INTO VotoPlatillo(IdPlatilloRestaurante,IdUsuario,Fecha,Voto)
    VALUES(idPlatilloTemp,idUsuarioTemp,fechaTemp,votoTemp);
	UPDATE Usuario
	SET Participacion=Participacion+1
	WHERE Usuario.IdUsuario = idUsuarioTemp;
END $$
DELIMITER ; 

DROP PROCEDURE IF EXISTS spAddVotoRestaurante;
DELIMITER $$
CREATE PROCEDURE spAddVotoRestaurante(
	IN idRestauranteTemp INT,
	IN idUsuarioTemp INT,
	IN votoTemp TINYINT
)
BEGIN
	DECLARE noExiste BOOL;
    SELECT funExistVotoRestaurante(idUsuarioTemp,idRestauranteTemp) INTO noExiste;
    IF noExiste=1 THEN		
		INSERT INTO VotoRestaurante(IdRestaurante,IdUsuario,Fecha,Voto)
		VALUES(idRestauranteTemp,idUsuarioTemp,CURRENT_DATE(),votoTemp);
		UPDATE Usuario
		SET Participacion=Participacion+1
		WHERE Usuario.IdUsuario = idUsuarioTemp;
	END IF;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddVotoRestauranteFecha;
DELIMITER $$
CREATE PROCEDURE spAddVotoRestauranteFecha(
	IN idRestauranteTemp INT,
	IN idUsuarioTemp INT,
    IN fechaTemp DATE,
	IN votoTemp TINYINT
)
BEGIN
	DECLARE noExiste BOOL;
    SELECT funExistVotoRestaurante(idUsuarioTemp,idRestauranteTemp) INTO noExiste;
    IF noExiste=1 THEN		
		INSERT INTO VotoRestaurante(IdRestaurante,IdUsuario,Fecha,Voto)
		VALUES(idRestauranteTemp,idUsuarioTemp,fechaTemp,votoTemp);
		UPDATE Usuario
		SET Participacion=Participacion+1
		WHERE Usuario.IdUsuario = idUsuarioTemp;
	END IF;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spAddVisita;
DELIMITER $$
CREATE PROCEDURE spAddVisita(
	IN idRestauranteTemp INT,
	IN fechaTemp DATE,
	IN horaTemp TIME,
	IN idUsuarioTemp INT
)
BEGIN
	INSERT INTO Visita(IdRestaurante,Fecha,Hora,IdUsuario)
    VALUES(idRestauranteTemp,fechaTemp,horaTemp,idUsuarioTemp);
    UPDATE Usuario
	SET Participacion=Participacion+1
	WHERE Usuario.IdUsuario = idUsuarioTemp;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGenerateHorario;
DELIMITER $$
CREATE PROCEDURE spGenerateHorario()
BEGIN
	CALL spAddHorario("08:00:00","20:00:00","Lunes","Viernes");
    CALL spAddHorario("07:30:00","19:00:00","Lunes","Viernes");
    CALL spAddHorario("08:00:00","19:00:00","Lunes","Viernes");
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGenerateRestaurante;
DELIMITER $$
CREATE PROCEDURE spGenerateRestaurante()
BEGIN
	CALL spAddRestaurante("Casa Luna","Comida para todo momento","Edificio K8",1);
    CALL spAddRestaurante("Soda El Lago","Cafe, batidos y mas","Edificio J1",3);
    CALL spAddRestaurante("Comedor Institucional","Comida accesible para todos","Edificio C1",2);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGenerateAdministrador;
DELIMITER $$
CREATE PROCEDURE spGenerateAdministrador()
BEGIN
	CALL spAddAdministrador("Cesar","Barrantes","lago@gmail.com","lago123",1);
    CALL spAddAdministrador("Patricia","Perez","comedor@gmail.com","come123",2);
    CALL spAddAdministrador("Vera","Mora","luna@gmail.com","luna123",3);
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGenerateCarrera;
DELIMITER $$
CREATE PROCEDURE spGenerateCarrera()
BEGIN
	CALL spAddCarrera("Ing.Computadores");
	CALL spAddCarrera("Ing.Electronica");
	CALL spAddCarrera("Ing.Computacion");
	CALL spAddCarrera("Ing.Mecatronica");
	CALL spAddCarrera("Ing.Mantenimiento");
	CALL spAddCarrera("Ing.Materiales");
	CALL spAddCarrera("Ing.Produccion");
	CALL spAddCarrera("Ing.Diseño");
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGenerateUsuario;
DELIMITER $$
CREATE PROCEDURE spGenerateUsuario()
BEGIN
	CALL spAddUsuario("Daniela","Hernandez","dani@gmail.com","abc",2015029225,0,1);
	CALL spAddUsuario("Alejandra","Castrillo","ale@gmail.com","abc",2015155759,0,1);
	CALL spAddUsuario("Esteban","Aguero","estape11@gmail.com","abc",2015097708,0,1);
	CALL spAddUsuario("Fiorella","Ulloa","fio@gmail.com","abc",2015023226,0,6);
	CALL spAddUsuario("Natalia","Morera","nati@gmail.com","abc",2015056431,0,8);
	CALL spAddUsuario("Miguel","Jimenez","migue@gmail.com","abc",2016102654,0,1);
	CALL spAddUsuario("Bernal","Villegas","berny@gmail.com","abc",2015014369,0,5);
	CALL spAddUsuario("Juan","Solano","js@gmail.com","abc",2015123456,0,4);
END $$ 
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGeneratePlatillo;
DELIMITER $$
CREATE PROCEDURE spGeneratePlatillo()
BEGIN
	CALL spAddPlatillo("Pinto","Pinto con huevo y platano maduro");
	CALL spAddPlatillo("Emparedado","Emparedado de pollo");
	CALL spAddPlatillo("Frutas","Taza con frutas mixtas de la temporada");
	CALL spAddPlatillo("Pasta","Spaghetti con salsa de tomate y pollo");
	CALL spAddPlatillo("Sopa Azteca","Sopa de tomate con tortillas");
	CALL spAddPlatillo("Arepas","Arepas con miel");
	CALL spAddPlatillo("Mini pizzas","Pizzas pequenas con salsa de tomate, peperoni y queso");
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGeneratePlatilloRestaurante;
DELIMITER $$
CREATE PROCEDURE spGeneratePlatilloRestaurante()
BEGIN
	CALL spAddPlatilloRestaurante(1,1,1500,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"09:00:00"); 
	CALL spAddPlatilloRestaurante(1,2,2000,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"09:00:00"); 
	CALL spAddPlatilloRestaurante(6,3,500,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"09:00:00"); 
	CALL spAddPlatilloRestaurante(2,1,2000,STR_TO_DATE("20-06-2017","%d-%m-%Y"),"04:00:00"); 
	CALL spAddPlatilloRestaurante(2,3,500,STR_TO_DATE("20-06-2017","%d-%m-%Y"),"09:00:00"); 
	CALL spAddPlatilloRestaurante(3,2,1000,STR_TO_DATE("22-08-2017","%d-%m-%Y"),"07:30:00"); 
	CALL spAddPlatilloRestaurante(3,3,600,STR_TO_DATE("23-09-2017","%d-%m-%Y"),"09:00:00"); 
	CALL spAddPlatilloRestaurante(4,1,2000,STR_TO_DATE("15-05-2017","%d-%m-%Y"),"11:20:00"); 
	CALL spAddPlatilloRestaurante(4,2,2300,STR_TO_DATE("19-03-2017","%d-%m-%Y"),"11:20:00"); 
	CALL spAddPlatilloRestaurante(5,3,700,STR_TO_DATE("05-03-2017","%d-%m-%Y"),"11:20:00"); 
	CALL spAddPlatilloRestaurante(6,3,300,STR_TO_DATE("21-04-2017","%d-%m-%Y"),"9:00:00"); 
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGenerateVisita;
DELIMITER $$
CREATE PROCEDURE spGenerateVisita()
BEGIN
	CALL spAddVisita(1,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"11:30:00",1);
	CALL spAddVisita(2,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"3:00:00",2);
	CALL spAddVisita(3,STR_TO_DATE("23-06-2017","%d-%m-%Y"),"08:00:00",3);
	CALL spAddVisita(3,STR_TO_DATE("29-09-2017","%d-%m-%Y"),"11:30:00",5);
	CALL spAddVisita(3,STR_TO_DATE("29-05-2017","%d-%m-%Y"),"3:00:00",5);
	CALL spAddVisita(2,STR_TO_DATE("29-09-2017","%d-%m-%Y"),"5:00:00",6);
	CALL spAddVisita(1,STR_TO_DATE("26-05-2017","%d-%m-%Y"),"3:00:00",8);
	CALL spAddVisita(1,STR_TO_DATE("29-05-2017","%d-%m-%Y"),"4:00:00",2);
	CALL spAddVisita(2,STR_TO_DATE("29-09-2017","%d-%m-%Y"),"3:00:00",2);
SELECT * FROM Visita ORDER BY IdRestaurante ASC;
END $$
DELIMITER ; 

DROP PROCEDURE IF EXISTS spGenerateVotoPlatillo;
DELIMITER $$
CREATE PROCEDURE spGenerateVotoPlatillo()
BEGIN
	CALL spAddVotoPlatilloFecha(1,1,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
	CALL spAddVotoPlatilloFecha(2,2,STR_TO_DATE("23-06-2017","%d-%m-%Y"),0);
	CALL spAddVotoPlatilloFecha(3,3,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(3,4,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
	CALL spAddVotoPlatilloFecha(3,5,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(3,5,STR_TO_DATE("11-10-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(3,6,STR_TO_DATE("29-09-2017","%d-%m-%Y"),0);
    CALL spAddVotoPlatilloFecha(3,2,STR_TO_DATE("15-09-2017","%d-%m-%Y"),0);
    CALL spAddVotoPlatilloFecha(3,1,STR_TO_DATE("17-09-2017","%d-%m-%Y"),0);
	CALL spAddVotoPlatilloFecha(3,5,STR_TO_DATE("29-05-2017","%d-%m-%Y"),0);
	CALL spAddVotoPlatilloFecha(2,6,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
	CALL spAddVotoPlatilloFecha(1,8,STR_TO_DATE("26-05-2017","%d-%m-%Y"),0);
	CALL spAddVotoPlatilloFecha(1,2,STR_TO_DATE("29-05-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(11,2,STR_TO_DATE("12-07-2017","%d-%m-%Y"),0);
    CALL spAddVotoPlatilloFecha(11,1,STR_TO_DATE("12-07-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(11,3,STR_TO_DATE("12-07-2017","%d-%m-%Y"),0);
    CALL spAddVotoPlatilloFecha(11,6,STR_TO_DATE("11-07-2017","%d-%m-%Y"),0);
    CALL spAddVotoPlatilloFecha(10,2,STR_TO_DATE("10-07-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(4,2,STR_TO_DATE("29-05-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(4,6,STR_TO_DATE("29-08-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(4,2,STR_TO_DATE("09-08-2017","%d-%m-%Y"),1);
    CALL spAddVotoPlatilloFecha(4,1,STR_TO_DATE("19-08-2017","%d-%m-%Y"),1);
	CALL spAddVotoPlatilloFecha(2,2,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
END $$
DELIMITER ; 

DROP PROCEDURE IF EXISTS spGenerateVotoRestaurante;
DELIMITER $$
CREATE PROCEDURE spGenerateVotoRestaurante()
BEGIN
	CALL spAddVotoRestauranteFecha(1,1,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
	CALL spAddVotoRestauranteFecha(2,2,STR_TO_DATE("23-06-2017","%d-%m-%Y"),0);
	CALL spAddVotoRestauranteFecha(3,3,STR_TO_DATE("23-06-2017","%d-%m-%Y"),1);
	CALL spAddVotoRestauranteFecha(3,5,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
	CALL spAddVotoRestauranteFecha(3,5,STR_TO_DATE("29-05-2017","%d-%m-%Y"),0);
	CALL spAddVotoRestauranteFecha(2,6,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
	CALL spAddVotoRestauranteFecha(1,8,STR_TO_DATE("26-05-2017","%d-%m-%Y"),0);
	CALL spAddVotoRestauranteFecha(1,2,STR_TO_DATE("29-05-2017","%d-%m-%Y"),1);
	CALL spAddVotoRestauranteFecha(2,2,STR_TO_DATE("29-09-2017","%d-%m-%Y"),1);
	SELECT * FROM VotoRestaurante ORDER BY IdRestaurante ASC;
END $$
DELIMITER ; 


DROP PROCEDURE IF EXISTS spGetRankingRestaurantes;
DELIMITER $$
CREATE PROCEDURE spGetRankingRestaurantes()
BEGIN
	SELECT R.Nombre, (SUM(V1.Voto=1)-SUM(V1.Voto=0))/(SUM(V1.Voto=1)+SUM(V1.Voto=0)) AS Ranking
    FROM Restaurante R
		LEFT JOIN VotoRestaurante AS V1 ON V1.IdRestaurante=R.IdRestaurante
    GROUP BY R.Nombre ORDER BY Ranking DESC; 
END $$
DELIMITER ; 